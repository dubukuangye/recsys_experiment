#!/usr/bin/env python
# -*- coding: utf-8 -*-

import operator
import math
import random
import multiprocessing
import os
import time
from collections import defaultdict
#from collections import Set


def start_process():
    None
    # print 'Starting',multiprocessing.current_process().name

data = []
train = {}
test = {}
train_items = []
item_popularity = {}
user_similarity_dict = {}
item_similarity_dict = {}

def init():
    train = {}
    test = {}
    train_items = []
    N = 0
    item_popularity = defaultdict(int)
    user_similarity_dict = {}    

def readDataSetFromFile(filename):
    data = []
    with open(filename, "r") as f:
      for line in f:
        # record = line.strip('\n').split('::')
        record = line.strip('\n').split(',')
        data.append([record[0], record[1]])
    return data

def splitData(data, k=1, M=8, seed=1):
    train = {}
    test = {}
    random.seed(seed)
    for user, item in data:
        if random.randint(0, M-1)==k:
            if user not in test:
                test[user] = [] 
            test[user].append(item)
        else:
            if user not in train:
                train[user] = [] 
            train[user].append(item)
    return train, test

def precision(all_rank, test):
    hit = 0
    all = 0
    for rank in all_rank:
        for user in rank.keys():
            all += len(rank[user])
            if user in test:
                hit += len(set(test[user]) & set(rank[user]))
    print 'precision.hit: ', hit
    print 'precision.all: ', all
    if all==0:
        print 'precision all is zero'
        return 0.0
    return hit/(all*1.0)

def recall(all_rank, test):
    hit = 0
    all = 0
       
    for rank in all_rank:
        for user in rank.keys():
            if user in test:
                hit += len(set(test[user]) & set(rank[user]))
                all += len(test[user])
    print 'recall.hit: ', hit
    print 'recall.all: ', all
    if all==0:
        print 'recall all is zero'
        return 0.0
    return hit/(all*1.0)

def coverage(all_rank, train_items):
    recommend_items = set()
    for rank in all_rank:
        for user in rank.keys():
            for item in rank[user]:
                recommend_items.add(item)
    return 1.0*len(recommend_items)/len(train_items)

def polularity(all_rank, item_popularity):
    ret = 0.0
    n = 0
    for rank in all_rank:
        for user in rank.keys():
            for item in rank[user]:
                ret += math.log(1+item_popularity[item])
                n += 1
    ret /= n*1.0
    return ret

def random_recommendation(user):
    rank = dict()
    no_interact_items = [i for i in train_items if i not in train[user]]
    rec_items = random.sample(no_interact_items, N)
    rank[user] = rec_items
    return rank

def most_popular_recommendation(user):
    candidate_item_list = [(v, k) for k, v in item_popularity.items() if k not in train[user]]
    candidate_item_list.sort()
    rank = dict()
    rank[user] = [k for v, k in candidate_item_list[-N:]]
    return rank

#def user_similarity(train):
#    item_users = {}
#    for u, items in train.items():
#        for i in items:
#            if i not in item_users:
#                item_users[i] = set()
#            item_users[i].add(u)
#    
#    C = {}
#    for i, users in item_users.items():
#        for u in users:
#            if u not in C: C[u] = defaultdict(int)
#            for v in users:
#                if u==v:
#                    continue
#                C[u][v] += 1
#    
#    user_similarity_dict = {}
#    for u, related_users in C.items():
#        user_similarity_dict[u] = {}
#        for v, cuv in related_users.items():
#            user_similarity_dict[u][v] = 1.0*cuv/math.sqrt( 1.0*len(train[u])*len(train[v]) )
#    
#    return user_similarity_dict

def user_similarity(train): # 对热门商品加了惩罚项
    item_users = {}
    for u, items in train.items():
        for i in items:
            if i not in item_users:
                item_users[i] = set()
            item_users[i].add(u)
    
    C = {}
    for i, users in item_users.items():
        for u in users:
            if u not in C: C[u] = defaultdict(int)
            for v in users:
                if u==v:
                    continue
                C[u][v] += 1.0/math.log(1+len(users))
    
    user_similarity_dict = {}
    for u, related_users in C.items():
        user_similarity_dict[u] = {}
        for v, cuv in related_users.items():
            user_similarity_dict[u][v] = 1.0*cuv/math.sqrt( 1.0*len(train[u])*len(train[v]) )
    
    return user_similarity_dict

def item_similarity(train): 
    C = {}
    for u, items in train.items():
        for i in items:
            if i not in C: C[i] = defaultdict(int)
            for j in items:
                if i==j:
                    continue
                C[i][j] += 1
    
    item_similarity_dict = {}
    for i, related_items in C.items():
        item_similarity_dict[i] = {}
        for j, cij in related_items.items():
            item_similarity_dict[i][j] = 1.0*cij/math.sqrt( 1.0*item_popularity[i]*item_popularity[j] )
    
    return item_similarity_dict


def user_cf_recommendation((user, N)):
    #user_similarity_dict = user_similarity(train)
    
    rating = defaultdict(int)
    for v, wuv in sorted(user_similarity_dict[user].iteritems(), key=operator.itemgetter(1), reverse=True)[0:N]:
        for i in train[v]:
            if i in train[user]:
                continue
            rating[i] += wuv
    
    rating_tuple = [(rate, i) for i, rate in rating.items()]
    rating_tuple.sort()
    rank = dict()
    rank[user] = [i for rate, i in rating_tuple[-10:]]
    return rank

def item_cf_recommendation((user, N)):
    rating = defaultdict(int)
    for i in train[user]:
        for j, wij in sorted(item_similarity_dict[i].iteritems(), key=operator.itemgetter(1), reverse=True)[0:N]:
            if j in train[user]:
                continue
            rating[j] += wij
    
    rating_tuple = [(rate, i) for i, rate in rating.items()]
    rating_tuple.sort()
    rank = dict()
    rank[user] = [i for rate, i in rating_tuple[-10:]]
    return rank

def get_item_popularity(train):
    item_popularity = defaultdict(int)
    for user, items in train.items():
        for item in items:
            item_popularity[item] += 1
    return item_popularity
        
def getAllItems(train):
    train_items = []
    for items in train.values():
        for item in items:
            if item not in train_items:
                train_items.append(item)
    return train_items

def recommend(N=10):
    user_N = [(u,N) for u in train.keys() if u in test]
#    all_rank = []
#    for user in users:
#        all_rank.append( user_cf_recommendation(user, user_similarity_dict, train, N) )

    pool=multiprocessing.Pool(initializer=start_process)
    #all_rank = pool.map(user_cf_recommendation, user_N)
    all_rank = pool.map(item_cf_recommendation, user_N)
    pool.close
    pool.join
            
    precision_score = precision(all_rank, test)
    recall_score = recall(all_rank, test)    
    coverage_score = coverage(all_rank, train_items)
    popularity_score = polularity(all_rank, item_popularity)
    return precision_score, recall_score, coverage_score, popularity_score

def print_result(r):
    print r

if __name__=='__main__':
    # data = readDataSetFromFile("ratings.dat")
    data = readDataSetFromFile("age_18_25.csv")
    train, test = splitData(data)
    train_items = getAllItems(train)
    item_popularity = get_item_popularity(train)
    # print 'item_popularity finished'
    #user_similarity_dict = user_similarity(train)
    #print 'user_similarity_dict finished'
    item_similarity_dict = item_similarity(train)
    # print 'item_similarity_dict finished'
    
    
    Ns = [5, 10, 20, 40, 80, 160] # N: select nearest N users, recommend N items
    # Ns = [80]
    for N in Ns:
        print str(N) + ': ' + str(recommend(N)) + '\n' 
            
        